package com.demo.test.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import com.demo.test.constant.Constant;
import com.demo.test.entity.PostRequest;
import com.demo.test.entity.ResponseWrapper;
import com.demo.test.entity.TagDTO;
import com.demo.test.model.Post;
import com.demo.test.model.Tag;
import com.demo.test.repository.PostRepository;
import com.demo.test.repository.TagRepository;
import com.demo.test.validator.JsonDateSerializerGson;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@ApplicationScoped
public class DemoService {

	private Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDateSerializerGson()).create();
	private Post post = new Post();
	private Tag tag = new Tag();
	
	@Inject
	PostRepository postRepo;
	
	@Inject
	TagRepository tagRepo;

	public ResponseWrapper addNewPost(PostRequest requestModel) {
		ResponseWrapper response = null;
		
		Set<Tag> tags = new HashSet<>();
		Set<Post> posts = new HashSet<>();
		post.setTitle(requestModel.getTitle());
		post.setContent(requestModel.getContent());
		
		try {
			if (requestModel.getTags() != null) {
				List<TagDTO> tagList = requestModel.getTags();
				for (TagDTO tagDto : tagList) {
					tag.setLabel(tagDto.getLabel());
					Optional<Tag> findTagByLabel = tagRepo.find("label", tagDto.getLabel())
							.singleResultOptional();
					if (findTagByLabel.isPresent()) {
						postRepo.persist(post);
						posts.add(post);
						tag.setPosts(posts);
						tagRepo.persist(tag);
					} else {
						tags.add(tag);
						post.setTags(tags);
						postRepo.persist(post);
					}
				}
			} else {
				postRepo.persist(post);
			}
			
			if (postRepo.isPersistent(post) && tagRepo.isPersistent(tag) || postRepo.isPersistent(post)) {
				response = new ResponseWrapper(Constant.SUCCESS, "Post successfully created", gson.toJson(post));
			} else {
				response = new ResponseWrapper(Constant.ERROR, "Can not save post into database");
			}
		} catch (Exception e) {
			response = new ResponseWrapper(Constant.ERROR, "", Response.status(Response.Status.BAD_REQUEST).build());
		}

		return response;
	}

}
