package com.demo.test.repository;

import javax.enterprise.context.ApplicationScoped;

import com.demo.test.model.Tag;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class TagRepository implements PanacheRepository<Tag> {

}
