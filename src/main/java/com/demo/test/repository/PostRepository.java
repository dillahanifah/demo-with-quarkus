package com.demo.test.repository;

import javax.enterprise.context.ApplicationScoped;

import com.demo.test.model.Post;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class PostRepository implements PanacheRepository<Post>{

}
