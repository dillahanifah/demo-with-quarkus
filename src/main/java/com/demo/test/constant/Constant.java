package com.demo.test.constant;

public class Constant {
	
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final String FAILED = "failed";
	public static final String NOT_FOUND = "error_not_found";
	public static final String IS_EXIST = "already exist";

}
