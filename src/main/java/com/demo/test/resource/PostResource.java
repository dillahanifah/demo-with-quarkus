package com.demo.test.resource;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.demo.test.entity.PostRequest;
import com.demo.test.entity.ResponseWrapper;
import com.demo.test.model.Post;
import com.demo.test.model.Tag;
import com.demo.test.repository.PostRepository;
import com.demo.test.service.DemoService;

@Path("/post")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PostResource {

	@Inject
	DemoService demoService;
	
	@Inject
	PostRepository postRepo;

	@POST
	@Path("/addPost")
	@Transactional
	public ResponseWrapper addPost(PostRequest request) {
		ResponseWrapper response = null;
		try {
			response = demoService.addNewPost(request);
		} catch (Exception e) {
			e.printStackTrace(new PrintWriter(new StringWriter()));
		}
		return response;
	}

	@GET
	@Path("/getAllPosts")
	public Response getAllPost() {
		try {
			List<Post> posts = postRepo.listAll();
			if (!posts.isEmpty()) {
				return Response.ok(posts).build();
			} else {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		} catch (Exception e) {
			e.printStackTrace(new PrintWriter(new StringWriter()));
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

	}

	@PUT
	@Path("updatePost")
	@Transactional
	public Response update(PostRequest request) {
		Optional<Post> findPostByTitle = postRepo.find("title", request.getTitle())
				.singleResultOptional();
		if (findPostByTitle.isPresent()) {
			Post p = findPostByTitle.get();
			Set<Tag> t = p.getTags();
			if (!t.isEmpty()) {
				for (Tag tag : t) {
					
				}
			} else {
				
			}
		}
		
		return Response.status(Response.Status.BAD_REQUEST).build();
	}

	@DELETE
	@Path("{id}")
	@Transactional
	public Response deleteById(@PathParam("id") Long id) {
		boolean deleteById = postRepo.deleteById(id);
		if (deleteById) {
			return Response.noContent().build();
		}
		return Response.status(Response.Status.BAD_REQUEST).build();
	}

}
