package com.demo.test.resource;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.demo.test.constant.Constant;
import com.demo.test.entity.ResponseWrapper;
import com.demo.test.entity.TagRequest;
import com.demo.test.model.Tag;
import com.demo.test.repository.TagRepository;
import com.demo.test.validator.JsonDateSerializerGson;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/tag")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TagsResource {
	
	private Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDateSerializerGson()).create();
	
	@Inject
	TagRepository tagRepo;

	@POST
	@Path("/addTag")
	@Transactional
	public ResponseWrapper addTag(TagRequest request) {
		ResponseWrapper response = null;
		try {
			Optional<Tag> findTagByLabel = tagRepo.find("label", request.getLabel())
					.singleResultOptional();
				if (findTagByLabel.isPresent()) {
					response = new ResponseWrapper(Constant.IS_EXIST, "Label is exist in database!");
				} else {
					Tag tag = new Tag();
					tag.setLabel(request.getLabel()); 
					tagRepo.persist(tag);
					if (tagRepo.isPersistent(tag)) {
						response = new ResponseWrapper(Constant.SUCCESS, "Label success save into database", gson.toJson(tag));
					} else {
						response = new ResponseWrapper(Constant.FAILED, "Failed add new label");
					}
				}
		} catch (Exception e) {
			response = new ResponseWrapper(Constant.ERROR, "", Response.status(Response.Status.BAD_REQUEST).build());
			e.printStackTrace(new PrintWriter(new StringWriter()));
		}
		
		return response;
	}

	@GET
	@Path("/getAllTags")
	public Response getAllTag() {
		try {
			List<Tag> tags = tagRepo.listAll();
			if (!tags.isEmpty()) {
				return Response.ok(tags).build();
			} else {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		} catch (Exception e) {
			e.printStackTrace(new PrintWriter(new StringWriter()));
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
	}
	
	@PUT
	@Path("/updateTag/{id}/{label}")
	@Transactional
	public ResponseWrapper updateTag(@PathParam("id") Long id, @PathParam("label") String label) {
		ResponseWrapper response = null;
		try {
			Tag tag = tagRepo.findById(id);
			if (tag == null) {
				response = new ResponseWrapper(Constant.NOT_FOUND, "Id is not found! Can not update label: " + label);
			} else {
				Optional<Tag> findTagByLabel = tagRepo.find("label", label)
						.singleResultOptional();
				if (findTagByLabel.isPresent()) {
					response = new ResponseWrapper(Constant.IS_EXIST, "Label is exist in database! Please input new label");
				} else {
					tag.setLabel(label);
					tagRepo.persist(tag);
					if (tagRepo.isPersistent(tag)) {
						response = new ResponseWrapper(Constant.SUCCESS, "Label success update into database", gson.toJson(tag));
					} else {
						response = new ResponseWrapper(Constant.FAILED, "Failed update label");
					}
				}
			}
		} catch (Exception e) {
			response = new ResponseWrapper(Constant.ERROR, "", Response.status(Response.Status.BAD_REQUEST).build());
			e.printStackTrace(new PrintWriter(new StringWriter()));
		}
		
		return response;
	}
	
	@DELETE
	@Path("/deleteTag/{id}")
	@Transactional
	public Response deleteTagById(@PathParam("id") Long id) {
		try {
			boolean deleteTagById = tagRepo.deleteById(id);
			if (deleteTagById) {
				return Response.noContent().build();
			} else {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		} catch (Exception e) {
			e.printStackTrace(new PrintWriter(new StringWriter()));
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
	}

}
