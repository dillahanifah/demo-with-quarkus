package com.demo.test.entity;

import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseWrapper {
	
	protected String result;
	protected String message;
	protected String json;
	protected Response response;
	
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}
	public Response getResponse() {
		return response;
	}
	public void setResponse(Response response) {
		this.response = response;
	}
	
	public ResponseWrapper(String result, String message) {
		super();
		this.result = result;
		this.message = message;
	}
	
	public ResponseWrapper(String result, String message, String json) {
		super();
		this.result = result;
		this.message = message;
		this.json = json;
	}
	
	public ResponseWrapper(String result, String message, Response response) {
		super();
		this.result = result;
		this.message = message;
		this.response = response;
	}

}
